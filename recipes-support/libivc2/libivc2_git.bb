SUMMARY = "libivc specific for dom0"
DESCRIPTION = "This recipe builds/provides libivc for dom0."
LIC_FILES_CHKSUM = "file://../../../LICENSE;md5=33f1e9b996445fae3abdec0dc53f884f"
LICENSE = "GPLv2"

DEPENDS = " \
    qtbase \
    libxenbe \
"

PROVIDES = "virtual/libivc"

PV = "2+git${SRCPV}"
SRC_URI = "git://gitlab.com/vglass/ivc.git;protocol=https;branch=master"
SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git/src/usivc/ivclib"

require recipes-qt/qt5/qt5.inc

RCONFLICTS_${PN} = "libivc"
RCONFLICTS_${PN}-dbg = "libivc-dbg"
RCONFLICTS_${PN}-dev = "libivc-dev"
