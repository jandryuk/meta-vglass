SUMMARY = "libivc specific for dom0"
DESCRIPTION = "This recipe builds/provides libivc for dom0."
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://../../../LICENSE;md5=33f1e9b996445fae3abdec0dc53f884f"

DEPENDS = " \
    qtbase \
    libxenbe \
"

PV = "0+git${SRCPV}"
SRC_URI = " \
    git://gitlab.com/vglass/ivc.git;protocol=https;branch=master \
    file://ivcdaemon.initscript \
"
SRCREV = "${AUTOREV}"

S = "${WORKDIR}/git/src/usivc/ivcdaemon"

require recipes-qt/qt5/qt5.inc
inherit update-rc.d

do_install_append() {
    install -d "${D}${sysconfdir}/init.d"
    install -m 755 "${WORKDIR}/ivcdaemon.initscript" "${D}${sysconfdir}/init.d/ivcdaemon"
}

INITSCRIPT_NAME = "ivcdaemon"
INITSCRIPT_PARAMS = "defaults 97 0"

# Not using -tools pkg-split.
PACKAGES_remove += "${PN}-tools"
FILES_${PN} += " \
    ${OE_QMAKE_PATH_BINS} \
"
